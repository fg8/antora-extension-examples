| isis.applib.annotation. +
action-layout.css-class-fa. +
patterns
| 
| Provides a mapping of patterns to font-awesome CSS classes, where the pattern is used to match against the name of the action method in order to determine a CSS class to use, for example on the action's menu icon if rendered by the Wicket viewer.

Providing a default set of patterns encourages a common set of verbs to be used.

The font awesome class for individual actions can be overridden using org.apache.isis.applib.annotation.ActionLayout#cssClassFa().


| isis.applib.annotation. +
action-layout.css-class. +
patterns
| 
| Provides a mapping of patterns to CSS classes, where the pattern is used to match against the name of the action method in order to determine a CSS class to use, for example on the action's button if rendered by the Wicket viewer.

Providing a default set of patterns encourages a common set of verbs to be used.

The CSS class for individual actions can be overridden using ``org.apache.isis.applib.annotation.ActionLayout#cssClass()``.


| isis.applib.annotation.action. +
command
| 
| The default for whether action invocations should be reified as a ``org.apache.isis.applib.services.command.Command`` using the ``org.apache.isis.applib.services.command.spi.CommandService``, possibly so that the actual execution of the action can be deferred until later (background execution) or replayed against a copy of the system.

In particular, the ``org.apache.isis.applib.services.command.CommandWithDto`` implementation of ``org.apache.isis.applib.services.command.Command`` represents the action invocation memento (obtained using ``CommandWithDto#asDto()``) as a ``org.apache.isis.schema.cmd.v2.CommandDto``.

This setting can be overridden on a case-by-case basis using ``org.apache.isis.applib.annotation.Action#command()``.


| isis.applib.annotation.action. +
domain-event.post-for-default
|  true
| Influences whether an ``org.apache.isis.applib.events.domain.ActionDomainEvent`` should be published (on the internal ``org.apache.isis.applib.services.eventbus.EventBusService``) whenever an action is being interacted with.

Up to five different events can be fired during an interaction, with the event's phase determining which (hide, disable, validate, executing and executed). Subscribers can influence the behaviour at each of these phases.

The algorithm for determining whether (and what type of) an event is actually sent depends on the value of the ``org.apache.isis.applib.annotation.Action#domainEvent()`` for the action in question:

* If set to some subtype of ActionDomainEvent.Noop, then _no_ event is sent.
* If set to some subtype of ActionDomainEvent.Default, then an event is sent _if and only if_ this configuration setting is set.
* If set to any other subtype, then an event _is_ sent.

