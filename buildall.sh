#!/bin/sh

ROOT=`pwd`

for p in antora-* antora-pdf/simple-pdfs
do
  echo $p
  cd $p
  rm -rf node_modules package-lock.json cache
  npm i --cache=cache/npm
  npm run build
  cd $ROOT
  echo $p done
done

echo done with all
