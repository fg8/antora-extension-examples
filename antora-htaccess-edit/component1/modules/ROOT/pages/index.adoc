= {page-component-title}

== Page Coordinates

page-component-name: {page-component-name}

page-component-version: {page-component-version}

page-module: {page-module}

page-relative: {page-relative}


== A page

There is no content in this example site.
The only point of interest is to compare the source `static/.htaccess` file and the edited `state/.htaccess-x` file.
